package com.example.jason.pocketmoneez;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddExpenseActivity extends AppCompatActivity {
    //#############################################################################################
    //# Instance variables
    float curTotal;
    boolean hasCurTotalChanged = false; //To tell if curTotal should be written to disk

    DatabaseHelper dbHelper;
    SQLiteDatabase expenseDb;

    Date todaysdate;
    SimpleDateFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);

        //Add back button the the actionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Read pocketMoney total passed in from ActivityMain and store in a variable
        if(getIntent().getExtras() != null){
            Bundle extras = getIntent().getExtras();
            curTotal = extras.getFloat("currentTotal");
            //Display pocketMoney total to top right textView
            UpdateTotalPmTextView();

            todaysdate = Calendar.getInstance().getTime(); //Get todays date
            formatter = new SimpleDateFormat("dd-MM-yyyy"); //Set date format

            //Instantiate database variables
            dbHelper = new DatabaseHelper(this);
            //Note: Because they can be long-running, be sure that you call getWritableDatabase() or
            // getReadableDatabase() in a background thread, such as with AsyncTask or IntentService.
            //dbWriteable = dbHelper.getWritableDatabase();
            //dbReadable = dbHelper.getReadableDatabase();

            //dbHelper.deleteExpenseTable(dbReadable); //Used to reset the data in the expense table

            //Read expenseHistory and display todays expenses if any
            DisplayExpenseHistory();
        }
    }

    @Override
    protected void onStop() {
        //Close the database if its opened
        if(expenseDb.isOpen()){ expenseDb.close(); }

        if(hasCurTotalChanged){ //Write current total to disk if its changed and hasn't been saved
            WritePmToFile();
            hasCurTotalChanged = false;
        }

        super.onStop();
    }

    /**
     * When the user presses the hardware Back button send back a Result to the calling intent(MainActivity)
     * so it knows the back button was pressed and it will need to read
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("readFileAgain", 1);
        setResult(RESULT_OK, intent);
        finish();

        //super.onBackPressed();
    }

    /**
     * Checks data from both EditText views are correct format and saves data to the SQLite database.
     * Also subtracts the amount entered from the current pocketMoney total and displays the new
     * value on screen.
     * @param view Add Expense button
     */
    public void AddExpense(View view){
        //Use a regex to check user has entered correct dollar value when tapping AddExpense button
        Pattern p = Pattern.compile("\\d{1,4}\\.\\d{2}");
        Matcher m;

        EditText newAmountET = findViewById(R.id.editTextEnterAmount);
        EditText newDescripET = findViewById(R.id.editText2EnterDescptn);

        //String newExpense = String.format("%.2f", newAmountET.getText());
        String newExpense = newAmountET.getText().toString();
        String newDescrptn = newDescripET.getText().toString();

        m = p.matcher(newExpense); //Check entered expense against REGEX

        //Check user has entered correct data
        if(m.matches() && !newDescrptn.equals("")){ //If a number is correct format and description entered
            //Subtract new expense from stored pocketMoney total and update display
            curTotal -= Float.parseFloat(newExpense);
            hasCurTotalChanged = true;
            UpdateTotalPmTextView();
            //Write new total to disk
            WritePmToFile();
            //Save new expense data to database
            SaveDataToDatabase(Float.parseFloat(newExpense), newDescrptn);

            //Clear ExpenseAmount editTextView and description editTextView
            newAmountET.setText("");
            newDescripET.setText("");

            //Query database to update the tableLayout to show new expense
            DisplayExpenseHistory();
        }
        else if(m.matches() && newDescrptn.equals("")){ //If number correct format but no description
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Must enter a description",
                    Toast.LENGTH_LONG);
            toast.show();
        }
        else if(!m.matches() && newDescrptn.equals("")){ //If number NOT correct format and no description
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Must enter a description and correct dollar value\neg. 1.34  10.34  100.34  1000.34",
                    Toast.LENGTH_LONG);
            toast.show();
        }
        else{ //Otherwise the number format is NOT correct
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Must enter a correct dollar value\neg. 1.34  10.34  100.34  1000.34",
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /**
     * Updates the CurrentTotal TextView with the value stored in curTotal and adds
     * a $ sign to the front as well as displays the float value to 2 decimal places
     */
    private void UpdateTotalPmTextView(){
        TextView currentTotal = findViewById(R.id.textView_CurrentTotal);
        currentTotal.setText("$" + String.format("%.2f", curTotal));
    }

    /**
     * Writes the current PocketMoney total to disk
     */
    private void WritePmToFile(){
        try{
            File pmTotal = new File(getApplicationContext().getFilesDir(), "pmTotal.txt");

            BufferedWriter writer = new BufferedWriter(new FileWriter(pmTotal, false));
            writer.write(String.format("%.2f", curTotal));
            writer.flush();
            writer.close();
            hasCurTotalChanged = false; //Reset flag

            //If the current total is less than or equal to zero disable the addExpense button
            if(curTotal <= 0.0f){
                Button btn = findViewById(R.id.button_AddExpense2);
                btn.setEnabled(false);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Saves data entered by user into the SQLite database
     * @param newExpense The expense entered by user
     * @param description The description entered by user
     */
    private void SaveDataToDatabase(Float newExpense, String description){
        expenseDb = dbHelper.getWritableDatabase();
        boolean insertSuccessful;
        Toast toast;
        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        //Add current date, new expense and description to database
        insertSuccessful = dbHelper.addExpense(expenseDb, formatter.format(todaysdate), newExpense, description);

        if(insertSuccessful){
            toast = Toast.makeText(this, "Values saved to database", Toast.LENGTH_LONG);
        }
        else{
            toast = Toast.makeText(this, "Error saving to database", Toast.LENGTH_LONG);
        }

        toast.show();
        expenseDb.close();
    }

    /**
     * Queries database for all expenses that match todays date
     */
    private void DisplayExpenseHistory(){
        expenseDb = dbHelper.getReadableDatabase();

        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String[] columns = {
                PocketMoneezContract.PmEntry.COLUMN_NAME_DATE,
                PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE,
                PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION};

        //Filter results WHERE "date" = 'todaysDate'
        String selection = PocketMoneezContract.PmEntry.COLUMN_NAME_DATE + " =? ";
        String[] selectionArgs = { formatter.format(todaysdate) };

        //How you want the results sorted in the resulting Cursor
        //String sortOrder = PocketMoneezContract.PmEntry._ID + " DESC";

        Cursor cursor = expenseDb.query(PocketMoneezContract.PmEntry.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        //Lists to store results of query
        List<String> dateList = new ArrayList<>();
        //List<String> expenseList = new ArrayList<>();
        List<Float> expenseList = new ArrayList<>();
        List<String> descriptList = new ArrayList<>();

        int newId;
        String item;
        Float fIteam;

        //Store the values from query into lists
        while(cursor.moveToNext()){
            //Store the date
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_DATE);
            item = cursor.getString(newId);
            dateList.add(item);
            //Store the Expense
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE);
            //item = cursor.getString(newId);
            fIteam = cursor.getFloat(newId);
            expenseList.add(fIteam);
            //Store the description
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION);
            item = cursor.getString(newId);
            descriptList.add(item);
        }

        //Close database objects
        cursor.close();
        expenseDb.close();
        //Display the values read from the database
        PopulateTodaysExpensesTl(dateList, expenseList, descriptList);
    }

    /**
     * Displays values read from a database to the TableView for user
     * @param dateList List of dates
     * @param expenseList List of Expenses
     * @param descripList List of Descriptions
     */
    private void PopulateTodaysExpensesTl(List<String> dateList, List<Float> expenseList, List<String> descripList){
        TableLayout mTl = findViewById(R.id.TableLo_TodaysExps);
        mTl.removeAllViewsInLayout(); //Clear any existing rows from TableLayout

        TextView newTv = new TextView(this);
        newTv.setMinWidth(150);
        TableRow newRow = new TableRow(this);
        newRow.setBackgroundColor(Color.parseColor("#FFC0CB"));
        //Setup Table column headings
        //Date
        //newTv.setText("Date");
        newTv.setText(R.string.Text_TableHeading_Date);
        newTv.setPadding(10, 0, 0, 0);
        newRow.addView(newTv);
        //Cost
        newTv = new TextView(this);
        //newTv.setText("Cost");
        newTv.setText(R.string.Text_TableHeading_Cost);
        newRow.addView(newTv);
        //Description
        newTv = new TextView(this);
        newTv.setPadding(20, 0, 0, 0);
        //newTv.setText("Description");
        newTv.setText(R.string.Text_TableHeading_Descrptn);
        newRow.addView(newTv);

        //mSv.addView(newRow);
        mTl.addView(newRow);

        //Insert a row from database into the TableLayout
        for (int i = 0; i < dateList.size(); i++){
            //Create new Row
            newRow = new TableRow(this);
            newRow.setId(i);
            //TextView to put the Date
            newTv = new TextView(this);
            newTv.setText(dateList.get(i));
            newTv.setPadding(10, 0, 0, 0);
            newTv.setTextSize(16);
            newRow.addView(newTv);
            //TextView to put the Expense
            newTv = new TextView(this);
            //newTv.setText(expenseList.get(i));
            newTv.setText(String.format("%.2f", expenseList.get(i)));
            newTv.setTextSize(16);
            newRow.addView(newTv);
            //TextView to put the Description
            newTv = new TextView(this);
            newTv.setText(descripList.get(i));
            newTv.setPadding(20, 0, 0, 0);
            newTv.setTextSize(16);
            newRow.addView(newTv);

            mTl.addView(newRow);

            /*
            //Allows user to select rows from the TableLayout
            newRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TableRow tempRow = (TableRow)v;
                    tempRow.setBackgroundColor(Color.parseColor("#A9A9A9"));
                    Log.d("jason", "A table row was tapped has index of : " + tempRow.getId());
                }
            });
            */
        }
    }

}
