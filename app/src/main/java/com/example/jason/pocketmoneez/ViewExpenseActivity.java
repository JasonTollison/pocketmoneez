package com.example.jason.pocketmoneez;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CalendarView;
import android.widget.TableLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//TODO
// - When returning to ViewExpenseActivity from StatisticsActivity nothing is displayed

public class ViewExpenseActivity extends AppCompatActivity {
    //#############################################################################################
    //# Instance variables
    DatabaseHelper dbHelper;
    SQLiteDatabase expenseDb;

    TableLayoutPopulator tableLayoutPopulator;

    String myDate; //Stores the current date selected on the CalenderView
    SimpleDateFormat formatter;

    String curTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("JasonTest", "#####################################");
        Log.d("JasonTest", "onCreate entered ViewExpense");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_expense);

        //Add back button the the actionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Setup CalenderView for use in the database queries
        final CalendarView myCal = findViewById(R.id.calendarView);
        //Get and store the current pmTotal from disk
        ReadPmFromFile();
        TextView currentPm = findViewById(R.id.textView_CurrentTotal);
        //Display the current pocketMoney total on thh top right of screen
        currentPm.setText("$" + curTotal);

        //Setup TableLayout ready to display data
        TableLayout mTl = findViewById(R.id.TableLo_Expenses);
        tableLayoutPopulator = new TableLayoutPopulator(mTl, this);

        //Instantiate database
        dbHelper = new DatabaseHelper(this);
        formatter = new SimpleDateFormat("dd-MM-yyyy"); //Set date format

        //Add a listener to the calenderView to display expense data when a different day is selected
        myCal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Log.d("JasonTest", "myCal tapped");
                //Make sure the day of the month has a leading zero if needed
                myDate = String.format("%02d", dayOfMonth) + "-" + Integer.toString(month + 1) + "-" + Integer.toString(year);
                DisplayExpenseHistory(myDate); //Display expenses for the day selected on the calender
            }
        });

        //Read expenseHistory and display todays expenses if any
        String todaysDate = formatter.format(Calendar.getInstance().getTime());
        DisplayExpenseHistory(todaysDate);
    }

    @Override
    protected void onStart() {
        Log.d("JasonTest", "onStart entered ViewExpense");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d("JasonTest", "onResume entered ViewExpense");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d("JasonTest", "onPause entered ViewExpense");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d("JasonTest", "onStop entered ViewExpense");
        super.onStop();
    }

    //#############################################################################################
    //# Private Methods

    /**
     * Reads from file pmTotal and stores the total pocketMoney.
     * Takes File to read from as parameter
     */
    private void ReadPmFromFile(){

        try{
            FileInputStream fileInput = getApplicationContext().openFileInput("pmTotal.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInput));
            String tempString;

            if((tempString = br.readLine()) != null){
                //Store the line from file
                curTotal = tempString;
            }

            br.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Queries database for all expenses that match date parameter passed in.
     * @param date The date you want the database query to find expenses for.
     */
    private void DisplayExpenseHistory(String date){
        Log.d("JasonTest", "Inside DisplayExpenseHistory() Gonna read from database" + date);
        expenseDb = dbHelper.getReadableDatabase();

        String[] columns = {
                PocketMoneezContract.PmEntry.COLUMN_NAME_DATE,
                PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE,
                PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION};

        //Filter results WHERE "date" = 'todaysDate'
        String selection = PocketMoneezContract.PmEntry.COLUMN_NAME_DATE + " =? ";
        //String[] selectionArgs = { formatter.format(todaysdate) };
        String[] selectionArgs = { date };

        //How you want the results sorted in the resulting Cursor
        //String sortOrder = PocketMoneezContract.PmEntry._ID + " DESC";

        Cursor cursor = expenseDb.query(PocketMoneezContract.PmEntry.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        //Lists to store results of query
        List<String> dateList = new ArrayList<>();
        List<Float> expenseList = new ArrayList<>();
        List<String> descriptList = new ArrayList<>();

        int newId;
        String item;
        Float fIteam;

        //Store the values from query into lists
        while(cursor.moveToNext()){
            //Store the date
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_DATE);
            item = cursor.getString(newId);
            dateList.add(item);
            //Store the Expense
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE);
            fIteam = cursor.getFloat(newId);
            expenseList.add(fIteam);
            //Store the description
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION);
            item = cursor.getString(newId);
            descriptList.add(item);
        }

        //Close database objects
        cursor.close();
        expenseDb.close();

        //Display the values read from the database to the TableLayout
        tableLayoutPopulator.addData(dateList, expenseList, descriptList);
    }

    //#############################################################################################
    //# Public Methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.viewexpense_activity_menu, menu);

        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menuItem_ShowStats){
            Log.d("JasonTest", "Now opening stats activity");
            Intent intent = new Intent(this, StatisticsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
