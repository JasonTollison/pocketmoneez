package com.example.jason.pocketmoneez;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//TODO
// - Should have a menu on actionBar with options
// - Todays expenses
// - Option to select a month
//      - Which makes a constraint visibile with all months(buttons). This can be displayed above the graph constraint.
//      - Tapping a month will show the

public class StatisticsActivity extends AppCompatActivity {
    //#############################################################################################
    //# Instance variables
    DatabaseHelper dbHelper;
    SQLiteDatabase expenseDb;

    SimpleDateFormat formatter;

    List<Float> expenseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //Back arrow top left of screen


        //Instantiate database
        dbHelper = new DatabaseHelper(this);
        formatter = new SimpleDateFormat("dd-MM-yyyy"); //Set date format
        String selectedDate = formatter.format(Calendar.getInstance().getTime());
        //Query database for todays expenses
        getTodaysExpenses(selectedDate);

        if(expenseList.size() > 0){
            //final String[] days = new String[4];
            final String[] labels = setLabelArray(expenseList.size()); //Create and store labels for the data
            //Create dataPoints for the graph from todays expenses
            LineGraphSeries<DataPoint> series = createNewDataSeries();

            GraphView myGraphView = new GraphView(this);
            //Data for graph
        /*
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0,2.00),
                new DataPoint(1, 15.00),
                new DataPoint(2,8.50),
                new DataPoint(3,45.75)
        });
        */
            myGraphView.addSeries(series);
            //Labels for the graph
            myGraphView.getGridLabelRenderer().setNumHorizontalLabels(expenseList.size());
            myGraphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(){
                @Override
                public String formatLabel(double value, boolean isValueX) {
                    if(isValueX){
                        return labels[(int)value]; //Display day from the days array
                    }
                    else {
                        return "$" + super.formatLabel(value, isValueX);
                    }
                }
            });

            ConstraintLayout constraintLayout = findViewById(R.id.ConstraintLo);
            constraintLayout.addView(myGraphView);
        }
        else{
            //Create a toast to inform user no data to show
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No expenses today",
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.statistics_activity_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.StatsMenu_TodayExpn){
            Log.d("JasonTest", "Todays expenses button tapped");
        }
        if(item.getItemId() == R.id.StatsMenu_ChooseMnth){
            Log.d("JasonTest", "Choose month button tapped");
        }
        return super.onOptionsItemSelected(item);
    }

    //#############################################################################################
    //# Private Methods

    /**
     * Method takes a list of expenses from database query and returns a series of datapoints to
     * use as data for a graph.
     * @return A LineGraphSeries of datapoints for a graph
     */
    private LineGraphSeries<DataPoint> createNewDataSeries(){
        DataPoint[] newSeries = new DataPoint[expenseList.size()];

        for(int i = 0; i < expenseList.size(); i ++){
            newSeries[i] = new DataPoint(i, expenseList.get(i));
        }

        return new LineGraphSeries<>(newSeries);
    }

    /**
     * Method creates a list of labels for x axis of a graph
     * @param arrayLength Number of labels to make
     * @return a String array of labels
     */
    private String[] setLabelArray(int arrayLength){
        String[] newLabelsArray = new String[arrayLength];

        for(int i = 0; i < arrayLength; i++){
            newLabelsArray[i] = "Item" + i;
        }

        return newLabelsArray;
    }

    /**
     * Method takes a date as String and queries sql database for all expenses with that date.
     * @param date date String to search database for.
     */
    private void getTodaysExpenses(String date){
        expenseDb = dbHelper.getReadableDatabase();

        String[] columns = { PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE };

        //Filter results WHERE "date" = 'todaysDate'
        String selection = PocketMoneezContract.PmEntry.COLUMN_NAME_DATE + " =? ";
        //String[] selectionArgs = { formatter.format(todaysdate) };
        String[] selectionArgs = { date };

        //How you want the results sorted in the resulting Cursor
        //String sortOrder = PocketMoneezContract.PmEntry._ID + " DESC";

        Cursor cursor = expenseDb.query(PocketMoneezContract.PmEntry.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        //Lists to store results of query
        ///List<String> dateList = new ArrayList<>();
        expenseList = new ArrayList<>();
        //List<String> descriptList = new ArrayList<>();

        int newId;
        //String item;
        Float fIteam;

        //Store the values from query into lists
        while(cursor.moveToNext()){
            //Store the date
            //newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_DATE);
            //item = cursor.getString(newId);
            //dateList.add(item);
            //Store the Expense
            newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE);
            fIteam = cursor.getFloat(newId);
            expenseList.add(fIteam);
            //Store the description
            //newId = cursor.getColumnIndexOrThrow(PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION);
            //item = cursor.getString(newId);
            //descriptList.add(item);
        }

        //Close database objects
        cursor.close();
        expenseDb.close();

        //Display the values read from the database to the TableLayout
        //tableLayoutPopulator.addData(dateList, expenseList, descriptList);
    }
}
