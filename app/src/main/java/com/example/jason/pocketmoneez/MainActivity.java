package com.example.jason.pocketmoneez;

// TODO
// - Add some sort of helper to alert user to set their total pocket money if it hasn't already
// - Create and add an icon for the app
// - Move the date query code into the DatabaseHelper class as its used twice and the code isn't much different.

// ----{ Extras }----
// - Remove date from tableLayout and just display the expense and the description?
// - Put contents into a scrollView. Cuts off bottom half of UI when changed to landscape mode.

// ----{ BUGS } -----

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    //#############################################################################################
    //# Instance variables
    //Standard Alert Dialog when user taps the Set Total option from action bar
    AlertDialog.Builder builder;
    AlertDialog dialogBox;
    //Alert Dialog for the overwrite confirmation
    AlertDialog.Builder builderOverwrite;
    AlertDialog dialogBoxOverwrite;

    boolean hasCorrectInput = false; //Flag for when user has input a new pocketMoney total
    boolean pressedSetButton = false; //Flag for when user has pressed the Set button from the top right menu

    //Keep track of users current pocketMoney total
    String curTotal;
    File pmTotal;

    Button addExpensesButton;
    Button viewExpensesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create an AlertDialog to allow user to enter a new Pocket Money total
        SetupAlertDialog();
        //Create AlertDialog to confirm user wants to overwrite pmTotal file if it already exists
        SetupAlertDialog2();

        //Find out if user has set a current pocketMoney total
        pmTotal = new File(getApplicationContext().getFilesDir(), "pmTotal.txt");
        //
        viewExpensesButton = findViewById(R.id.button_ViewExpenses);
        addExpensesButton = findViewById(R.id.button_AddExpense2);

        //If the file exists read the file and display current pocketMoney total to a textView
        if(pmTotal.exists()){
            ReadPmFromFile();
            UpdateRemainingPmTextView();
        }
        else{
            curTotal = "0000.00";
            addExpensesButton.setEnabled(false);
            viewExpensesButton.setEnabled(false);
        }
    }

    /**
     * Method is called when MainActivity is restarted when the user presses the hardware Back
     * button from AddExpenseActivity. It then re-reads the pocketMoney file and updates the
     * display incase the total has changed.
     * @param requestCode The code used when starting AddExpenseActivity
     * @param resultCode The result that was returned by the AddEpenseActivity
     * @param data The actual data returned from AddExepnseActivity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                ReadPmFromFile();
                UpdateRemainingPmTextView();
            }
        }
    }

    //#############################################################################################
    //# Private Methods
    /**
     * Creates a new AlertDialog to display when user clicks on the Set Toal button
     * from the action bar (Top right)
     */
    private void SetupAlertDialog(){
        //Creates and setup the Alertdialog
        builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("Set total pocket money");
        builder.setMessage("Enter the total amount of pocketmoney to start from");
        //Setup an input box to use in the alertDialog
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(input);
        //When user presses Set button check user input is correct and write new total to file
        builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Looking for a number anywhere from 1 to 4 digits followed by a decimal point and 2 digits
                Pattern p = Pattern.compile("\\d{1,4}\\.\\d{2}");
                Matcher m;

                String newTotal = input.getText().toString();
                m = p.matcher(newTotal); //Check input against regex

                //If a number has been entered Check if its in correct format
                if(m.matches()){
                    hasCorrectInput = true;
                    curTotal = newTotal;
                }
                else{ hasCorrectInput = false; }

                pressedSetButton = true;
            }
        });

        //When user presses the Cancel button on the AlertDialog
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                hasCorrectInput = true;
                pressedSetButton = false;
                dialog.dismiss();
            }
        });

        //When AlertDialog is dissmised check if correct dollar value was entered
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if(pressedSetButton){
                    //If the number format is incorrect bring dialog box back up
                    if(hasCorrectInput == false){
                        dialogBox.show();
                        //Create a toast to inform user of incorrect usage
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Must enter a correct dollar value\neg. 1.34  10.34  100.34  1000.34",
                                Toast.LENGTH_LONG);
                        toast.show();
                    }
                    //If the user has correct number format and pm total file exists
                    else if(hasCorrectInput && pmTotal.exists()){
                        dialogBoxOverwrite.show(); //Alert user they are about to overwrite existing file
                    }
                    //If the user has correct number format and a pm total file doesnt exist
                    else if(hasCorrectInput && !pmTotal.exists()){
                        WritePmToFile();
                    }
                }
                //Otherwise the user just wants to cancel the dialog box
                else{ dialogInterface.cancel();}
            }
        });

        dialogBox = builder.create();
    }

    /**
     * Creates a new AlertDialog to inform user they are about to overwrite existing file.
     * Gives user the option to either continue with the overwrite or cancel and keep existing pm total.
     */
    private void SetupAlertDialog2(){
        //Creates and setup the Alertdialog
        builderOverwrite = new AlertDialog.Builder(MainActivity.this);

        builderOverwrite.setTitle("Overwrite existing pocket money");
        builderOverwrite.setMessage("Pocket money total already exists are you sure you want to overwrite?");

        //When user pressed the Overwrite button write the new Pocket Money total to file and display on screen
        builderOverwrite.setPositiveButton("Overwrite", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WritePmToFile();
            }
        });

        //When user presses the Cancel button just close the Alert Dialog
        builderOverwrite.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogBoxOverwrite = builderOverwrite.create();
    }

    /**
     * Reads from file pmTotal and stores the total pocketMoney.
     * Takes File to read from as parameter
     */
    private void ReadPmFromFile(){

        try{
            FileInputStream fileInput = getApplicationContext().openFileInput("pmTotal.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInput));
            String tempString;

            if((tempString = br.readLine()) != null){
                //Store the line from file
                curTotal = tempString;
            }

            br.close();

            //If the total is zero disable the AddExpense button
            if(Float.parseFloat(curTotal) <= 0.0){
                addExpensesButton.setEnabled(false);
                //viewExpensesButton.setEnabled(false);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Writes the current PocketMoney total to disk then displays the total to the user.
     * If the file already exists the user is alerted before writing.
     */
    private void WritePmToFile(){
        try{
            //Check if the pmFile needs to be created
            if(!pmTotal.exists()){ pmTotal.createNewFile(); }

            BufferedWriter writer = new BufferedWriter(new FileWriter(pmTotal, false));
            writer.write(curTotal);
            writer.flush();
            writer.close();
            //Update the Remaining Pocket Money textView
            UpdateRemainingPmTextView();

            //If the total is zero and the addExpense button is disabled, enable the AddExpense button
            if(Float.parseFloat(curTotal) > 0.0 && !addExpensesButton.isEnabled()){
                addExpensesButton.setEnabled(true);
                //viewExpensesButton.setEnabled(true);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Sets the RemainingPMOutput TextView to display the current PocketMoney
     */
    private void UpdateRemainingPmTextView(){
        TextView currentPm = findViewById(R.id.textView_RemainingPMOutput);
        currentPm.setText("$" + curTotal);
    }

    //#############################################################################################
    //# Public Methods
    /**
     * Opens up the AddExpense activity
     * @param view Add Expense button
     */
    public void StartAddExpense(View view){
        Intent intent = new Intent(this, AddExpenseActivity.class);
        intent.putExtra("currentTotal", Float.parseFloat(curTotal)); //Send current pocketMoney total through
        //Start the activity for result incase user presses hardware Back button
        startActivityForResult(intent, 1);
    }

    /**
     * Opens up the ViewExpense activity
     * @param view View Expenses button
     */
    public void StartViewExpense(View view){
        Intent intent = new Intent(this, ViewExpenseActivity.class);
        intent.putExtra("currentTotal", curTotal); //Send current pocketMoney total through
        startActivity(intent);
    }

    /**
     * Creates a menu item at the top right of the actionBar.
     * has no icon.
     * @param menu Three dots menu button
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_activity_menu, menu);

        return true;
    }

    /**
     * Once the Set Total button is pressed(top right on actionBar) open an alert dialog
     * to let the user enter a starting amount for their pocket money
     * @param item the menu button pressed by the user
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.set_PocketMny){
            dialogBox.show(); //Show the alert dialog
        }
        return super.onOptionsItemSelected(item);
    }
}
