package com.example.jason.pocketmoneez;

import android.provider.BaseColumns;

/**
 * Class that holds constants that define names for database tables, and columns.
 * Makes it easier having them in one place
 */
public final class PocketMoneezContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private PocketMoneezContract() {}

        /* Inner class that defines the table contents */
        public static class PmEntry implements BaseColumns {
            public static final String TABLE_NAME = "expenseHistory";
            public static final String COLUMN_NAME_DATE = "date";
            public static final String COLUMN_NAME_EXPENSE = "expense";
            public static final String COLUMN_NAME_DESCRIPTION = "description";
        }
}
