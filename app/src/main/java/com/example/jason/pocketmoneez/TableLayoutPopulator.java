package com.example.jason.pocketmoneez;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

/**
 * Takes data and displays it on a TableLayout in three columns each with a label
 * Date, Cost and Description. The background color is set to pink with a minimum width of 150
 */
class TableLayoutPopulator {
    //#############################################################################################
    //# Instance variables
    TableLayout tableLayout;
    Context context;

    //#############################################################################################
    //# Constructor
    /**
     * Stores the TableLayout and Context parameters
     * @param tableLayout TableLayout to display data
     * @param context Context that has the TableLayout
     */
    public TableLayoutPopulator(TableLayout tableLayout, Context context){
        this.tableLayout = tableLayout;
        this.context = context;
    }

    //#############################################################################################
    //# Public Methods

    /**
     * Displays values from three lists to the TableView, each list represents
     * a column on the TableLayout.
     * @param dateList List of dates
     * @param expenseList List of expenses
     * @param descripList List of descriptions
     */
    public void addData(List<String> dateList, List<Float> expenseList, List<String> descripList){
        Log.d("jasonTest", "Made it DisplayTodayExpenses method");
        tableLayout.removeAllViewsInLayout(); //Make sure TableView is cleared before adding rows

        TextView newTv = new TextView(context);
        newTv.setMinWidth(150);
        TableRow newRow = new TableRow(context);
        newRow.setBackgroundColor(Color.parseColor("#FFC0CB"));
        //Setup Table column headings
        //Date
        newTv.setText(R.string.Text_TableHeading_Date);
        newTv.setPadding(10, 0, 0, 0);
        newRow.addView(newTv);
        //Cost
        newTv = new TextView(context);
        newTv.setText(R.string.Text_TableHeading_Cost);
        newRow.addView(newTv);
        //Description
        newTv = new TextView(context);
        newTv.setPadding(20, 0, 0, 0);
        newTv.setText(R.string.Text_TableHeading_Descrptn);
        newRow.addView(newTv);

        tableLayout.addView(newRow);

        //Insert a row from database into the TableLayout
        for (int i = 0; i < dateList.size(); i++){
            //Create new Row
            newRow = new TableRow(context);
            newRow.setId(i);
            //TextView to put the Date
            newTv = new TextView(context);
            newTv.setText(dateList.get(i));
            newTv.setPadding(10, 0, 0, 0);
            newTv.setTextSize(16);
            newRow.addView(newTv);
            //TextView to put the Expense
            newTv = new TextView(context);
            //newTv.setText(expenseList.get(i));
            newTv.setText(String.format("%.2f", expenseList.get(i)));
            newTv.setTextSize(16);
            newRow.addView(newTv);
            //TextView to put the Description
            newTv = new TextView(context);
            newTv.setText(descripList.get(i));
            newTv.setPadding(20, 0, 0, 0);
            newTv.setTextSize(16);
            newRow.addView(newTv);

            tableLayout.addView(newRow);
        }
    }

    /*
    //Allows user to select rows from the TableLayout
    newRow.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TableRow tempRow = (TableRow)v;
            tempRow.setBackgroundColor(Color.parseColor("#A9A9A9"));
            Log.d("jason", "A table row was tapped has index of : " + tempRow.getId());
        }
    });
    */
}
