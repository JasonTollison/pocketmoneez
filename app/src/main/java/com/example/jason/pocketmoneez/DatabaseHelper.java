package com.example.jason.pocketmoneez;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "pocketMoneezData.db";
    private static final int DB_VERSION = 1;
    //Queries to create and delete the expenseHistory table
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PocketMoneezContract.PmEntry.TABLE_NAME + " (" +
                    PocketMoneezContract.PmEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    PocketMoneezContract.PmEntry.COLUMN_NAME_DATE + " DATE," +
                    PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE + " MONEY," +
                    PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION + " VARCHAR)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PocketMoneezContract.PmEntry.TABLE_NAME;

    //Constructor
    public DatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_ENTRIES); //Execute the table creation query
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_DELETE_ENTRIES); //Execute the delete query

        onCreate(db);
    }

    public void deleteExpenseTable(SQLiteDatabase db){
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Takes values and stores into the database
     * @param db A database object
     * @param currentDate The date to insert into database
     * @param expense The cost of an item to insert into database
     * @param description The description of an item to insert into database
     * @return boolean weather the insert query to database was successful
     */
    public boolean addExpense(SQLiteDatabase db, String currentDate, Float expense, String description){
        long insertComplete;
        ContentValues contentValues = new ContentValues();
        //Insert user values into database
        contentValues.put(PocketMoneezContract.PmEntry.COLUMN_NAME_DATE, currentDate);
        contentValues.put(PocketMoneezContract.PmEntry.COLUMN_NAME_EXPENSE, expense);
        contentValues.put(PocketMoneezContract.PmEntry.COLUMN_NAME_DESCRIPTION, description);
        insertComplete = db.insert(PocketMoneezContract.PmEntry.TABLE_NAME, null, contentValues);

        if(insertComplete > 0){ return true; }
        else return false;
    }
}
